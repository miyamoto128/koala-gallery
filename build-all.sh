#! /bin/bash

echo "Ensure diesel_cli is installed..."
cd koala-back
cargo install diesel_cli
echo "..done"

echo "Execute migration scripts..."
diesel migration run
cd -
echo "..done"

echo "(re)Build Docker-compose services..."
docker-compose build
docker-compose images
echo "..done"
