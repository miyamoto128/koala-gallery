# Getting start

Depends on the following client library:

* libpq for the PostgreSQL backend

Build and execute:
```
$ cargo build
$ cargo run
```

Call <locahost:8080/status> to check the webapp is alive.

# Run migration scripts

## Pre-requisite, install diesel CLI

By default diesel depends on the following client libraries:

 * libpq for the PostgreSQL backend
 * libmysqlclient for the Mysql backend
 * libsqlite3 for the SQlite backend

```
$ cargo install diesel_cli
```

## Build database structure
Execute migration SQL:
```
$ diesel migration run
```

## More info
```
$ diesel migration help
```

https://diesel.rs/guides/getting-started.html
