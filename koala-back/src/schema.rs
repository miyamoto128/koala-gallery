table! {
    images (uuid) {
        uuid -> Varchar,
        name -> Varchar,
        url -> Varchar,
        created_at -> Timestamptz,
    }
}
