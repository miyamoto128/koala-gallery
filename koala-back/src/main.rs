//! Actix Web Diesel integration example
//!
//! Diesel does not support tokio, so we have to run it in separate threads using the web::block
//! function which offloads blocking code (like Diesel's) in order to not block the server's thread.

#[macro_use]
extern crate diesel;

use actix_web::{get, middleware, post, web, App, Error, HttpResponse, HttpServer, Responder};
use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
use uuid::Uuid;

mod actions;
mod models;
mod schema;

type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;

/// List all images.
#[get("/images")]
async fn list_images(pool: web::Data<DbPool>) -> Result<HttpResponse, Error> {
    // use web::block to offload blocking Diesel code without blocking server thread
    let images = web::block(move || {
        let conn = pool.get()?;
        actions::list_all_images(&conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(images))
}

/// Finds image by UID.
#[get("/image/{image_id}")]
async fn get_image(
    pool: web::Data<DbPool>,
    image_uid: web::Path<Uuid>,
) -> Result<HttpResponse, Error> {
    let image_uid = image_uid.into_inner();

    // use web::block to offload blocking Diesel code without blocking server thread
    let image = web::block(move || {
        let conn = pool.get()?;
        actions::find_image_by_uuid(image_uid, &conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    if let Some(image) = image {
        Ok(HttpResponse::Ok().json(image))
    } else {
        let res = HttpResponse::NotFound().body(format!("No image found with uid: {}", image_uid));
        Ok(res)
    }
}

/// Inserts new image with name defined in form.
#[post("/image")]
async fn add_image(
    pool: web::Data<DbPool>,
    form: web::Json<models::NewImage>,
) -> Result<HttpResponse, Error> {
    // use web::block to offload blocking Diesel code without blocking server thread
    let image = web::block(move || {
        let conn = pool.get()?;
        actions::insert_new_image(&form.name, &form.url, &conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(image))
}

/// Is alive
#[get("/status")]
async fn is_alive() -> impl Responder {
    HttpResponse::Ok().body("back is up")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    // set up database connection pool
    let conn_spec = std::env::var("DATABASE_URL").expect("DATABASE_URL");
    let manager = ConnectionManager::<PgConnection>::new(conn_spec);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

    log::info!("starting HTTP server at http://localhost:8080");

    // Start HTTP server
    HttpServer::new(move || {
        App::new()
            // set up DB pool to be used with web::Data<Pool> extractor
            .app_data(web::Data::new(pool.clone()))
            .wrap(middleware::Logger::default())
            .service(list_images)
            .service(get_image)
            .service(add_image)
            .service(is_alive)
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::test;

    #[actix_web::test]
    async fn image_routes() {
        std::env::set_var("RUST_LOG", "actix_web=debug");
        env_logger::init();
        dotenv::dotenv().ok();

        let connspec = std::env::var("DATABASE_URL").expect("DATABASE_URL");
        let manager = ConnectionManager::<PgConnection>::new(connspec);
        let pool = r2d2::Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let mut app = test::init_service(
            App::new()
                .app_data(web::Data::new(pool.clone()))
                .wrap(middleware::Logger::default())
                .service(list_images)
                .service(get_image)
                .service(add_image),
        )
        .await;

        // Insert an image
        let req = test::TestRequest::post()
            .uri("/image")
            .set_json(&models::NewImage {
                name: "Test image".to_owned(),
                url: "test_image_url.io".to_owned(),
            })
            .to_request();

        let resp: models::Image = test::call_and_read_body_json(&mut app, req).await;

        assert_eq!(resp.name, "Test image");
        let new_image_uuid = resp.uuid;

        // List all images
        let req = test::TestRequest::get().uri("/images").to_request();

        let resp: Vec<models::Image> = test::call_and_read_body_json(&mut app, req).await;

        assert!(resp.len() >= 1);
        assert_eq!(resp.last().unwrap().name, "Test image");

        // Get an image
        let req = test::TestRequest::get()
            .uri(&format!("/image/{}", new_image_uuid))
            .to_request();

        let resp: models::Image = test::call_and_read_body_json(&mut app, req).await;

        assert_eq!(resp.name, "Test image");

        // Delete new image from table
        use crate::schema::images::dsl::*;
        diesel::delete(images.filter(uuid.eq(new_image_uuid)))
            .execute(&pool.get().expect("couldn't get db connection from pool"))
            .expect("couldn't delete test image from table");
    }
}
