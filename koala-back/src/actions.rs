use diesel::prelude::*;
use uuid::Uuid;

use crate::models;

type DbError = Box<dyn std::error::Error + Send + Sync>;

/// Run query using Diesel to list all images.
pub fn list_all_images(conn: &PgConnection) -> Result<Vec<models::Image>, DbError> {
    use crate::schema::images::dsl::*;

    let all_images = images
        .select((uuid, name, url))
        .order(created_at.desc())
        .load::<models::Image>(conn)?;
    Ok(all_images)
}

/// Run query using Diesel to find image by uuid and return it.
pub fn find_image_by_uuid(
    searched_uuid: Uuid,
    conn: &PgConnection,
) -> Result<Option<models::Image>, DbError> {
    use crate::schema::images::dsl::*;

    let image = images
        .select((uuid, name, url))
        .filter(uuid.eq(searched_uuid.to_string()))
        .first::<models::Image>(conn)
        .optional()?;

    Ok(image)
}

/// Run query using Diesel to insert a new database row and return the result.
pub fn insert_new_image(
    img_name: &str,
    img_url: &str,
    conn: &PgConnection,
) -> Result<models::Image, DbError> {
    use crate::schema::images::dsl::*;

    let new_image = models::Image {
        uuid: Uuid::new_v4().to_string(),
        name: img_name.to_owned(),
        url: img_url.to_owned(),
    };

    diesel::insert_into(images)
        .values(&new_image)
        .execute(conn)?;

    Ok(new_image)
}
