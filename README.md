# Koala-gallery

Web-application that display drawing and painting as a gallery.


# Execute

Execute
```sh
$ ./build_all.sh
```
at first launch.

Execute
```sh
$ ./start-all.sh
```
to execute the overall services.


Then, open http://localhost in your web-browser.


# Terminate

Execute
```sh
$ ./stop-all.sh
```
to terminate the running services.
