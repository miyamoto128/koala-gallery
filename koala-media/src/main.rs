use actix_files as fs;
use actix_web::{
    error, get, http, post, web, App, Error, HttpRequest, HttpResponse, HttpServer, Responder,
};
use env_logger::Env;
use futures::StreamExt;

mod storage;

const ROOT_DIR: &str = "static";
const MAX_BODY_SIZE: usize = 10_000_000;

#[post("/")]
async fn add(req: HttpRequest, mut body: web::Payload) -> Result<HttpResponse, Error> {
    // Check Content-Length
    match req.headers().get(http::header::CONTENT_LENGTH) {
        Some(header_value) => {
            let content_length = header_value.to_str().unwrap().parse::<usize>().unwrap();
            if content_length > MAX_BODY_SIZE {
                log::error!(
                    "Content-Length: {} greater than maximum allowed: {}",
                    content_length,
                    MAX_BODY_SIZE
                );
                return Err(error::ErrorBadRequest("Body too large"));
            }
        }
        None => {
            log::warn!("No Content-Length");
        }
    }

    // Read body
    let mut bytes = web::BytesMut::new();
    while let Some(chunk) = body.next().await {
        let chunk = chunk?;
        if (bytes.len() + chunk.len()) > MAX_BODY_SIZE {
            return Err(error::ErrorBadRequest("Body too large"));
        }
        bytes.extend_from_slice(&chunk);
    }

    // Create storage file
    match storage::create_file(ROOT_DIR, bytes.to_vec()) {
        Ok(file_name) => Ok(HttpResponse::Ok().body(format!(
            "received {} bytes -> file: {}",
            bytes.len(),
            file_name,
        ))),
        Err(what) => Err(error::ErrorBadRequest(what)),
    }
}

/// Is alive
#[get("/status")]
async fn is_alive() -> impl Responder {
    HttpResponse::Ok().body("media is up")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    HttpServer::new(|| {
        App::new()
            .service(is_alive)
            .service(add)
            .service(fs::Files::new("/", ROOT_DIR).show_files_listing())
    })
    .bind(("0.0.0.0", 8888))?
    .run()
    .await
}
