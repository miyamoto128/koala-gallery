extern crate tree_magic;

use derive_more::{Display, Error};
use std::{fs, path::Path};
use uuid::Uuid;

#[derive(Debug, Display, Error)]
pub enum StorageError {
    #[display(fmt = "Mime type not supported.")]
    UnsupportedMimeType,

    #[display(fmt = "An internal error occurred. Please try again later.")]
    InternalError,
}

pub type StorageResult = Result<String, StorageError>;

pub fn create_file(root_dir: &str, bytes: Vec<u8>) -> StorageResult {
    let mime = tree_magic::from_u8(&bytes);
    log::info!("Recognised mime type {}", mime);

    let file_ext = mime_to_extension(&mime)?;
    let file_full_name = format!(
        "{}.{}",
        Uuid::new_v4().to_string().replace("-", "/"),
        file_ext
    );
    let file_path = Path::new(root_dir).join(&file_full_name);

    // Ensure file does not exist
    if file_path.exists() {
        log::error!("File {} already exists", file_path.display());
        return Err(StorageError::InternalError);
    }

    // Create parent folders
    let file_folders = file_path.parent().unwrap();
    match fs::create_dir_all(file_folders) {
        Ok(()) => log::info!("Create directory {}", file_folders.display()),
        Err(what) => {
            log::error!(
                "Failed to create directory {}, {}",
                file_folders.display(),
                what
            );
            return Err(StorageError::InternalError);
        }
    }

    // Write media file
    match fs::write(&file_path, bytes) {
        Ok(()) => log::info!("Write file {}", file_path.display()),
        Err(what) => {
            log::error!("Failed to write file {}, {}", file_path.display(), what);
            return Err(StorageError::InternalError);
        }
    }

    Ok(file_full_name)
}

fn mime_to_extension(mime: &str) -> StorageResult {
    match mime {
        "image/gif" => Ok("gif".to_string()),
        "image/jpeg" => Ok("jpg".to_string()),
        "image/png" => Ok("png".to_string()),
        _ => {
            log::warn!("Mime type {} not supported", mime);
            Err(StorageError::UnsupportedMimeType)
        }
    }
}
