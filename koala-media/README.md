# Getting start
Build and execute:
```
$ cargo build
$ cargo run
```

Call <locahost:8888/status> to check the webapp is alive.
